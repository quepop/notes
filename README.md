# Notes

A simple note-taking application with a frontend written in pure javascript,html and css and a backend written in node.js.

## Prerequisites

To run the app You need node.js and nedb npm package. Here is a command to install it:

```
npm install nedb
```

To start the app cd into main directory and execute the following command:

```
node server.js
```

## How to use 

The app stores your sets of notes (boards) as tables in a database. To create a new "board" or load an existing one, open the app in your browser (localhost:3000) and type its name. Your notes behave much like windows in a stacking window manager - you can stack, resize, create, edit, move and select single or multiple notes at once. To select mulpitle notes at once you click and hold the left mouse button and move your mouse to change the size of your selection - then you can move them just like you would a single note.   
