const defaults = {height: 150, width: 150, left: 75, top: 75};
const selectedBackgroundColor ="#ffeeee", defaultBackgroundColor = "#eeeeff", unit = "px", addUnitTo = ["left", "top", "width", "height"], absFollowing = ["width", "height"];
const buttonParameters = {"/edit.png": {click: editNote}, "/remove.png": {click: removeNote}, "/resize.png": {mousedown: startResizing}}
let selected, current, dbName;

function init(obj) {
    document.body.innerHTML = "";
    selected = [];
    document.title = (dbName = obj.path)
    createTag("img",imgAttributes("/add.png"),{click: addNote});
    createTag("div",{class: "counterStyle"}).innerHTML = " Ilość stworzonych magnesów: <p>0</p><br>Obecna ilość: <p>0</p>"
    for(note of obj.notes) addNote({},note.style,note.content,note._id);
    document.getElementsByTagName("p")[0].innerHTML = obj.total;
    document.body.onmousedown = startSelecting;
    window.onmouseup = stopCurrentAction;
} 

function addNote(event,styleObj = defaults, noteContent = "<div>Nowa kartka!</div>", iden = "") {
    document.getElementsByTagName("p")[0].innerHTML = parseInt(document.getElementsByTagName("p")[0].innerHTML) + 1
    let currentIndex = document.getElementsByTagName("p")[1].innerHTML = document.getElementsByClassName("noteStyle").length + 1;
    (current = createTag("div",{class: "noteStyle"},{mousedown: startMoving},Object.assign({},styleObj,{backgroundColor: defaultBackgroundColor}))).innerHTML = noteContent
    if(iden == "") current.style.zIndex = currentIndex, sendData({path: dbName, type: "addNote", zIndex: currentIndex, content: noteContent},(target) => {current.id = target.newId}) 
    else current.id = iden
    for(let path in buttonParameters) createTag("img",imgAttributes(path),buttonParameters[path],{},current);
}

function createTag(tag,attributes = {}, eventHandlers = {}, styles = {}, container = document.body) {
    let newTag = container.appendChild(document.createElement(tag))
    for(let attribute in attributes) newTag.setAttribute(attribute, attributes[attribute])
    for(let eventName in eventHandlers) newTag["on"+eventName] = eventHandlers[eventName];
    mergeObj(newTag.style,styles)
    return newTag
}

function mergeObj(first,second) {
    for(let key in second) first[key] = (absFollowing.indexOf(key)+1 ? Math.abs(second[key]) : second[key]) + (addUnitTo.indexOf(key)+1 ? unit : (typeof second[key] == "string" ? "" : 0)); 
}

function getVal(styleName, object = current) {return parseInt(object.style[styleName])};

function imgAttributes(path) {return {src: path, ondragstart: "return false;"}}

function editNote() {
    let editor = pell.init({element: createTag("div",{class: "textStyle"},{},{},createTag("div",{class: "protectionStyle"})), onChange: () => {}})
    editor.lastChild.innerHTML = this.parentNode.firstChild.innerHTML;
    createTag("img",imgAttributes("/save.png"),{click: () => {sendData({path: dbName, type:"updateContent", id: this.parentNode.id, content: editor.lastChild.innerHTML}), this.parentNode.firstChild.innerHTML = editor.lastChild.innerHTML, document.getElementsByClassName("protectionStyle")[0].remove()}},{},editor.firstChild);
    createTag("img",imgAttributes("/cancel.png"),{click: () => {document.getElementsByClassName("protectionStyle")[0].remove()}},{},editor.firstChild);
}

function removeNote() {
    sendData({path: dbName, type: "removeNote", id: this.parentNode.id})
    this.parentNode.remove();
    document.getElementsByTagName("p")[1].innerHTML = document.getElementsByClassName("noteStyle").length;
    for(note of document.getElementsByClassName("noteStyle")) if(getVal("zIndex",note) > getVal("zIndex",this.parentNode)) sendData({path: dbName, type: "updateLayer", id: note.id, zIndex: (note.style.zIndex = getVal("zIndex",note) - 1)}) 
    setSelectionState(selected,false);
}

function startResizing(event) {
    mergeObj(current = this.parentNode,{imgX: getVal("left") + getVal("width") - event.clientX, imgY: getVal("top") + getVal("height") - event.clientY})
    window.onmousemove = updateNoteSize
}

function updateNoteSize(event) {
    let nextWidth = event.clientX - getVal("left") + current.imgX, nextHeight = event.clientY - getVal("top") + current.imgY;
    mergeObj(current.style,{width: nextWidth > defaults.width ? nextWidth : defaults.width, height: nextHeight > defaults.height ? nextHeight : defaults.height})
}

function startSelecting(event) {
    if(event.composedPath()[0].tagName != "DIV" && event.composedPath()[1].tagName != "DIV") setSelectionState(selected,false);
    if(event.composedPath()[0].tagName != "BODY") return;
    mergeObj(current = createTag("div",{class: "selectionStyle"}), {startingX: event.clientX, startingY: event.clientY})
    window.onmousemove = updateSelectionSize;
}

function updateSelectionSize(event) {
    let nextWidth = event.clientX - current.startingX, nextHeight = event.clientY - current.startingY, intersecting = [];
    mergeObj(current.style, {width: nextWidth, height: nextHeight, left: nextWidth < 0 ? event.clientX : current.startingX, top: nextHeight < 0 ? event.clientY : current.startingY})
    for(note of document.getElementsByClassName("noteStyle")) if(getVal("left",note)+getVal("width",note) >= getVal("left") && getVal("left")+getVal("width") >= getVal("left",note) && getVal("top",note)+getVal("height",note) >= getVal("top") && getVal("top")+getVal("height") >= getVal("top",note)) intersecting.push(note)
    setSelectionState(selected,false), setSelectionState(intersecting,true)
}

function startMoving(event) {
    if(!(selected.indexOf(this)+1) || event.composedPath()[0].tagName == "IMG") setSelectionState(selected, false), setSelectionState([this], true)
    let leftIndex = 0, rightIndex = 0, toSend = [], sortedNotes = Array.prototype.slice.call(document.getElementsByClassName("noteStyle")).sort(function(a,b){return getVal("zIndex",a)<getVal("zIndex",b)}); selected.sort(function(a,b){return getVal("zIndex",a)<getVal("zIndex",b)})
    for(note of sortedNotes) {if(selected[leftIndex] == note) note.style.zIndex = sortedNotes.length - leftIndex++, toSend.push(note.id); else note.style.zIndex = sortedNotes.length - selected.length - rightIndex++}
    sendData({path: dbName, type:"updateLayer", notes: toSend})
    if(event.composedPath()[0].tagName == "IMG") return
    current = {startingX: event.clientX, startingY: event.clientY}
    for(note of selected) mergeObj(note,{offsetX: event.clientX - getVal("left",note), offsetY: event.clientY - getVal("top",note)})
    window.onmousemove = updateNotesPosition
}

function updateNotesPosition(event) {
    for(note of selected) mergeObj(note.style,{left: event.clientX - note.offsetX, top: event.clientY - note.offsetY})
}

function stopCurrentAction(event) {
    switch(window.onmousemove) {
        case updateSelectionSize: current.remove()
            break;
        case updateNotesPosition: if(current.startingX == event.clientX && current.startingY == event.clientY) break;
	    let toSend = []; for(note of selected) toSend.push(note.id); sendData(Object.assign({path: dbName, type:"moveNotes",endX: event.clientX, endY: event.clientY, notes: toSend},current))
            break;
        case updateNoteSize: for(note of selected) sendData({path: dbName, type:"updateSize", id: note.id, width: getVal("width",note), height: getVal("height",note)})
            break;
    }
    window.onmousemove = undefined
}

function setSelectionState(notes,mode) {
    selected = mode ? notes : []
    for(note of notes) note.style.backgroundColor = mode ? selectedBackgroundColor : defaultBackgroundColor
}

function sendData(obj, success = () => {}) {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "", true);
    xmlhttp.setRequestHeader("Content-Type","application/json")
    xmlhttp.overrideMimeType("application/json");
    xmlhttp.onreadystatechange = function () {if(xmlhttp.readyState == 4 && xmlhttp.status == 200) success(JSON.parse(xmlhttp.responseText))}
    xmlhttp.send(JSON.stringify(obj)); 
}

