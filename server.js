var http = require("http");
var fs = require("fs");
var Datastore = require('nedb')
var contentTypes = {js: 'application/javascript', css: 'text/css', png: 'image/png', html: 'text/html'}
var databases = {}
var startingPositions = {}
var defaultStyle = {height:150,width:150,left:75,top:75};

var server = http.createServer(function(req,res){
    if(req.method == "POST") {
        var allData = ""
        req.on("data", function (data) {allData += data;})
        req.on("end", function (data) {
            var finish = JSON.parse(allData)
            var db = databases[finish.path] == undefined ? (databases[finish.path] = new Datastore({filename: "dbs/" + finish.path + ".db", autoload: true})) : databases[finish.path]
            switch(finish.type) {
                case "init": 
                    db.findOne({type: "total"}, (err, totalRec) => {
                        if(!totalRec) db.insert({type: "total", value: 0}, (err,newTotalRec) => { res.end(JSON.stringify({total: 0, notes: []})) })
                        else db.find({type: "note"}, (err, notesRec) => { res.end(JSON.stringify({total: totalRec.value, notes: notesRec})) })
                    })
                    break;
                case "addNote":
                    db.insert({type: "note", style: Object.assign({},defaultStyle,{zIndex: finish.zIndex}), content: finish.content}, (err,noteRec) => { db.update({type: "total"}, {$inc: {value: 1}}, {}, () => { res.end(JSON.stringify({newId: noteRec._id})) }) })
                    break;
                case "removeNote":
                    db.remove({_id: finish.id}, {}, () => { res.end(JSON.stringify("removed")) })
                    break;
                case "updateLayer":
                    db.find({type: "note"}).sort({"style.zIndex": -1}).exec((err,noteRecs) => {
                        var leftIndex = 0, rightIndex = 0
                        noteRecs.forEach((item,index) => {
                            db.update({_id: item._id}, {$set: {"style.zIndex": item._id == finish.notes[leftIndex] ?  noteRecs.length - leftIndex++ :  noteRecs.length - finish.notes.length - rightIndex++ }}, {}, () => {
                            if(index == noteRecs.length -1) res.end(JSON.stringify("updatedLayers"))
                            })                 
                        })
                    })
                    break;
                case "moveNotes":
                    finish.notes.forEach((item,index) => {    
                        db.update({_id: item}, {$inc: {"style.left": finish.endX - finish.startingX, "style.top": finish.endY - finish.startingY}}, {}, () => {
                            if(index == finish.notes.length -1) res.end(JSON.stringify("stoppedMoving"))
                        })                                             
                    })
                    break;
                case "updateSize":
                    db.update({_id: finish.id}, {$set: {"style.width": finish.width, "style.height": finish.height}}, {}, () => { res.end(JSON.stringify("resized")) })
                    break;
                case "updateContent":
                    db.update({_id: finish.id}, {$set: {content: finish.content}}, {}, () => { res.end(JSON.stringify("updated")) }) 
                    break;
            }
        })
        return;
    }
    var file = "static" + (req.url == "/" ? "/index.html" : req.url)
    fs.readFile(file, function (error, data) {
        if(!error) {
            res.writeHead(200, { 'Content-Type': contentTypes[file.split(".").pop()]});
            res.write(data);
        }
        else {
            res.writeHead(404, { 'Content-Type': 'text/html' });            
            res.write("<h1>404 - Not found</h1>");
        }
        res.end();
    });
})

server.listen(3000, function(){
   console.log("serwer startuje na porcie 3000")
});


